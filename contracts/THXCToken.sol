// SPDX-License-Identifier: MIT

pragma solidity 0.8.24;

import './interfaces/THXCTokenInterface.sol';

contract THXCToken is THXCTokenInterface {
    /// Custom Errors
    error ERC20InvalidSender(address sender);
    error ERC20InvalidReceiver(address receiver);
    error ERC20InsufficientBalance(address sender, uint256 balance, uint256 needed);
    error ERC20InsufficientAllowance(address spender, uint256 allowance, uint256 needed);
    error ERC20InvalidSpender(address spender);
    error MismatchedArrays(uint256 len1, uint256 len2);
    error TooManyRecipients(uint256 count, uint256 maxCount);
    error TooManySpenders(uint256 count, uint256 maxCount);
    error BulkValueTooHigh(uint256 value, uint256 maxValue);

    /* This is a slight change to the ERC20 base standard.
    function totalSupply() constant returns (uint256 supply);
    is replaced with:
    uint256 public totalSupply;
    This automatically creates a getter function for the totalSupply.
    This is moved to the base contract since public getter functions are not
    currently recognised as an implementation of the matching abstract
    function by the compiler.
    */
    /// total amount of tokens
    uint256 public totalSupply;

    uint256 private constant MAX_UINT256 = ~uint256(0);
    uint256 private BULK_MAX_VALUE;
    uint32 private constant BULK_MAX_COUNT = 100;

    uint256 private txCounter;

    event BulkTransfer(uint256 indexed _txId, uint256 _bulkCount);
    event BulkApproval(uint256 indexed _txId, uint256 _bulkCount);

    mapping(address => uint256) private balances;
    mapping(address => mapping(address => uint256)) private allowed;

    string public name;
    uint8 public decimals;
    string public symbol;

    constructor(
        uint256 _totalSupply,
        string memory _name,
        uint8 _decimals,
        string memory _symbol
    ) {
        totalSupply = _totalSupply * (10 ** uint256(_decimals));
        name = _name;
        decimals = _decimals;
        symbol = _symbol;
        balances[msg.sender] = totalSupply;
        BULK_MAX_VALUE = (10 ** 9) * (10 ** uint256(_decimals));
        txCounter = 0;
    }

    /**
     * @dev Transfer tokens for a specified address
     *
     * @param _to The address that will receive the tokens.
     * @param _value The amount of tokens to send.
     *
     * Requirements:
     *
     * - `_to` cannot be the zero address.
     * - `_to` cannot be the the contract.
     * - the caller must have a balance of at least `_value`.
     *
     * @return success A boolean that indicates if the operation was successful.
     */
    function transfer(
        address _to,
        uint256 _value
    ) external override returns (bool success) {
        if (_to == address(0)) {
            revert ERC20InvalidReceiver(address(0));
        }
        if (_to == address(this)) {
            revert ERC20InvalidReceiver(address(this));
        }

        uint256 fromBalance = balances[msg.sender];
        if (fromBalance < _value) {
            revert ERC20InsufficientBalance(msg.sender, fromBalance, _value);
        }

        return _transfer(_to, _value);
    }

    function transferFrom(
        address _spender,
        address _to,
        uint256 _value
    ) external override returns (bool success) {
        if (_spender == address(0)) {
            revert ERC20InvalidSpender(address(0));
        }

        uint256 _allowance = allowed[_spender][msg.sender];
        if (_allowance < _value) {
            revert ERC20InsufficientAllowance(_spender, _allowance, _value);
        }

        if (_to == address(0)) {
            revert ERC20InvalidReceiver(address(0));

        }

        balances[_spender] -= _value;
        balances[_to] += _value;

        if (_allowance != MAX_UINT256) {
            // Special case to approve unlimited transfers
           allowed[_spender][msg.sender] -= _value;
        }

        emit Transfer(_spender, _to, _value);
        return true;
    }

    function balanceOf(
        address _owner
    ) external view override returns (uint256 balance) {
        return balances[_owner];
    }

    function approve(
        address _spender,
        uint256 _value
    ) external override returns (bool success) {
        if (_spender == address(0)) {
            revert ERC20InvalidSpender(address(0));
        }

        allowed[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value); //solhint-disable-line indent, no-unused-vars
        return true;
    }

    function increaseApproval(
        address _spender,
        uint256 _delta
    ) public returns (bool success) {
        if (_spender == address(0)) {
            revert ERC20InvalidSpender(address(0));
        }

        uint256 _oldValue = allowed[msg.sender][_spender];

        uint256 newValue;
        unchecked {
            newValue = _oldValue + _delta;
        }

        if (newValue < _oldValue || newValue == MAX_UINT256) {
            // Truncate upon overflow.
            allowed[msg.sender][_spender] = MAX_UINT256 - 1;
        } else {
            allowed[msg.sender][_spender] = newValue;
        }

        emit Approval(msg.sender, _spender, allowed[msg.sender][_spender]);
        return true;
    }

    function decreaseApproval(
        address _spender,
        uint256 _delta
    ) external returns (bool success) {
        if (_spender == address(0)) {
            revert ERC20InvalidSpender(address(0));
        }

        uint256 _oldValue = allowed[msg.sender][_spender];
        if (_delta > _oldValue) {
            // Truncate upon overflow.
            allowed[msg.sender][_spender] = 0;
        } else {
            allowed[msg.sender][_spender] =
                _oldValue -
                _delta;
        }
        emit Approval(msg.sender, _spender, allowed[msg.sender][_spender]);
        return true;
    }

    function allowance(
        address _owner,
        address _spender
    ) external view override returns (uint256 remaining) {
        return allowed[_owner][_spender];
    }

    /**
     * @dev Transfer tokens to multiple addresses
     * if one of the transfers fails the whole transaction will not be reverted
     *
     * @param _tos The addresses that will receive the tokens.
     * @param _values The amount of tokens to send per address
     *     *
     * @return _bulkCount The sum of transfers that were successful
     */
    function transferBulk(
        address[] memory _tos,
        uint256[] memory _values
    ) external override returns (uint256 _bulkCount) {
        if (_tos.length != _values.length) {
            revert MismatchedArrays(_tos.length, _values.length);
        }

        if (_tos.length > BULK_MAX_COUNT) {
            revert TooManyRecipients(_tos.length, BULK_MAX_COUNT);
        }

        uint256 _bulkValue = 0;
        for (uint256 j = 0; j < _tos.length; ++j) {
            _bulkValue += _values[j];
        }
        if (_bulkValue > BULK_MAX_VALUE) {
            revert BulkValueTooHigh(_bulkValue, BULK_MAX_VALUE);
        }

        txCounter++; // Increment the counter for each new transaction
        bool _success;
        for (uint256 i = 0; i < _tos.length; ++i) {
            _success = transferQuiet(_tos[i], _values[i]);
            if (_success) {
                _bulkCount++;
            }
        }
        emit BulkTransfer(txCounter, _bulkCount);
        return _bulkCount;
    }

    function increaseApprovalBulk(
        address[] memory _spenders,
        uint256[] memory _values
    ) external returns (uint256 _bulkCount) {
        if (_spenders.length != _values.length) {
            revert MismatchedArrays(_spenders.length, _values.length);
        }

        if (_spenders.length > BULK_MAX_COUNT) {
            revert TooManySpenders(_spenders.length, BULK_MAX_COUNT);
        }

        uint256 _bulkValue = 0;
        for (uint256 j = 0; j < _spenders.length; ++j) {
            _bulkValue += _values[j];
        }
        if (_bulkValue > BULK_MAX_VALUE) {
            revert BulkValueTooHigh(_bulkValue, BULK_MAX_VALUE);
        }

        txCounter++; // Increment the counter for each new transaction
        bool _success;
        for (uint256 i = 0; i < _spenders.length; ++i) {
            _success = increaseApproval(_spenders[i], _values[i]);
            if (_success) {
                _bulkCount++;
            }
        }
        emit BulkApproval(txCounter, _bulkCount);
        return _bulkCount;
    }

    /**
     * @dev like transfer, but it fails silently
     * on transferBulk we have to fail silently if one of the transfers fails, if not the whole transaction will fail
     *
     * @param _to The address that will receive the tokens.
     * @param _value The amount of tokens to send.
     *
     * Requirements:
     *
     * - `_to` cannot be the zero address.
     * - `_to` cannot be the the contract.
     * - the caller must have a balance of at least `_value`.
     *
     * @return success A boolean that indicates if the operation was successful.
     */
    function transferQuiet(
        address _to,
        uint256 _value
    ) internal returns (bool success) {
        if (
            _to == address(0) ||
            _to == address(this) ||
            balances[msg.sender] < _value
        ) return false; // Preclude burning tokens to uninitialized address, or sending tokens to the contract.

        return _transfer(_to, _value);
    }

    /**
     * @dev internal transfer function
     *
     * @param _to The address that will receive the tokens.
     * @param _value The amount of tokens to send.
     *
     * @return success A boolean that indicates if the operation was successful.
     */
    function _transfer(
        address _to,
        uint256 _value
    ) internal returns (bool success) {
        balances[msg.sender] -= _value;
        balances[_to] += _value;

        emit Transfer(msg.sender, _to, _value);
        return true;
    }
}
