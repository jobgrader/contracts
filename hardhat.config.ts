import { HardhatUserConfig } from 'hardhat/config';
import '@nomicfoundation/hardhat-chai-matchers'
import '@nomicfoundation/hardhat-toolbox';
import '@openzeppelin/hardhat-upgrades';

const config: HardhatUserConfig = {
  solidity: {
    compilers: [
      {
        version: '0.6.2',
      },
      {
        version: '0.8.24',
        settings: { optimizer: { enabled: true, runs: 1000000 } },
      },
    ],
  }
};

export default config;
