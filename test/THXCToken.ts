import { expect } from 'chai';
import { ethers } from 'hardhat';
import { MaxUint256, parseUnits, Signer, ZeroAddress } from 'ethers';
import { THXCToken } from '../typechain-types';


let owner: Signer,
  accounts: Signer[];

let token: THXCToken;

async function deployToken() {
  // Deploy THXCToken Contract
  const THXCToken = await ethers.getContractFactory(
    'contracts/THXCToken.sol:THXCToken'
  );
  token = (await THXCToken.deploy(
    1000000000,
    'Thanks Coin',
    18,
    'THXC'
  )) as THXCToken;
}


describe('THXCToken', function () {
  this.beforeAll(async () => {
    [
      owner,
      ...accounts
    ] = await ethers.getSigners();
  });

  describe('deployment', () => {
    before(async () => {
      await deployToken();
    });

    it('Token should be deployed', async () => {
      const result = await token.getAddress();
      expect(result).not.to.be.undefined;
    });

    it('Name should be Thanks Coin', async () => {
      const result = await token.name();
      expect(result).to.equal('Thanks Coin');
    });

    it('Decimals should be 18', async () => {
      const result = await token.decimals();
      expect(result).to.equal(18);
    });

    it('Total supply should be 1000000000 * (10 ** 18)', async () => {
      const result = await token.totalSupply();
      expect(result).to.equal(1000000000000000000000000000n);
    });

    it('Total supply should be 1000000000 * (10 ** 18)', async () => {
      const result = await token.totalSupply();
      expect(result).to.equal(1000000000000000000000000000n);
    });

  });

  describe('Transfer', () => {

    it('Should transfer 100 tokens from owner to accounts[0]', async () => {
      await token.connect(owner).transfer(accounts[0], 100);
      const result = await token.balanceOf(accounts[0]);
      expect(result).to.equal(100);
    });

    it('accounts[2] should transfer 50 tokens from accounts[0] to accounts[1] But Spender allowance to low', async () => {
      const call = token.connect(accounts[2]).transferFrom(accounts[0], accounts[1], 50);
      await expect(call).to.be.revertedWithCustomError(token, 'ERC20InsufficientAllowance')
        .withArgs(
          accounts[0],
          0,
          50
        );
    });

    it('accounts[2] should transfer 50 tokens from accounts[0] to accounts[1]', async () => {
      await token.connect(accounts[0]).approve(await accounts[2], 100);

      const result = await token.connect(accounts[2]).transferFrom(accounts[0], accounts[1], 50);
      const balanceAccount3 = await token.balanceOf(accounts[0]);
      expect(balanceAccount3).to.equal(50);
      const balanceAccount4 = await token.balanceOf(accounts[1]);
      expect(balanceAccount4).to.equal(50);
    });

    it('Try to send tokens from owner to contract', async () => {
      const call = token.connect(owner).transfer(await token.getAddress(), 100);
      await expect(call).to.be.revertedWithCustomError(token, 'ERC20InvalidReceiver').withArgs(await token.getAddress());
    });

    it('Try to send tokens from accounts[0] to contract', async () => {
      const call = token.connect(accounts[0]).transfer(await token.getAddress(), 100);
      await expect(call).to.be.revertedWithCustomError(token, 'ERC20InvalidReceiver').withArgs(await token.getAddress());
    });

    it('Try to send tokens from owner to zero address', async () => {
      const call = token.connect(owner).transfer(ZeroAddress, 100);
      await expect(call).to.be.revertedWithCustomError(token, 'ERC20InvalidReceiver').withArgs(ZeroAddress);
    });

    it('Try to send tokens from accounts[0] zero address', async () => {
      const call = token.connect(accounts[0]).transfer(ZeroAddress, 100);
      await expect(call).to.be.revertedWithCustomError(token, 'ERC20InvalidReceiver').withArgs(ZeroAddress);
    });

    it('Try to send tokens from accounts[0] to accounts[1] but balance exceed', async () => {
      const call = token.connect(accounts[0]).transfer(accounts[1], 100);

      await expect(call).to.be.revertedWithCustomError(token, 'ERC20InsufficientBalance')
        .withArgs(
          accounts[0],
          50,
          100
        );
    });
  });

  describe('TransferFrom', () => {
    it('send 100 tokens from owner to accounts[1]', async () => {
      await token.connect(owner).transfer(accounts[1], 100);
      const result = await token.balanceOf(accounts[1]);
      expect(result).to.equal(150);
    });

    it('spender accounts[0] Should transfer 100 tokens from accounts[1] to accounts[2] but has insufficient allowance', async () => {
      const call = token.connect(accounts[0]).transferFrom(accounts[1], accounts[2], 100);

      await expect(call).to.be.revertedWithCustomError(token, 'ERC20InsufficientAllowance')
        .withArgs(
          accounts[1],
          0,
          100
        );
    });

    it('spender accounts[0] Should transfer 100 tokens from accounts[1] to accounts[2] but has insufficient allowance', async () => {
      const call = token.connect(accounts[0]).transferFrom(accounts[1], accounts[2], 100);

      await expect(call).to.be.revertedWithCustomError(token, 'ERC20InsufficientAllowance')
        .withArgs(
          accounts[1],
          0,
          100
        );
    });

    it('spender accounts[0] Should transfer 100 tokens from accounts[1] to zero address which is not allowed', async () => {
      await token.connect(accounts[1]).approve(await accounts[0], 100);

      const call = token.connect(accounts[0]).transferFrom(accounts[1], ZeroAddress, 100);

      await expect(call).to.be.revertedWithCustomError(token, 'ERC20InvalidReceiver')
        .withArgs(
          ZeroAddress
        );
    });

    it("should revert on transferFrom when spender is address(0)", async function () {

      await expect(token.connect(accounts[0]).transferFrom(ZeroAddress, accounts[0], 100))
          .to.be.revertedWithCustomError(token, "ERC20InvalidSpender")
          .withArgs(ZeroAddress);
    });

  });

  describe('approve', () => {
    it('accounts[0] try to approve 100 tokens for zero address which is invalid', async () => {
      const call = token.connect(accounts[0]).approve(ZeroAddress, 100);

      await expect(call).to.be.revertedWithCustomError(token, 'ERC20InvalidSpender')
        .withArgs(
          ZeroAddress
        );
    });

    it('accounts[4] should have 0 allowance for accouts[3]', async () => {
      const result = await token.allowance(accounts[3], accounts[4]);
      await expect(result).to.equal(0);
    });

    it('accounts[4] set allowance for 10 tokens to accouts[3]', async () => {
      await token.connect(accounts[4]).approve(accounts[3], 10);

      const result = await token.allowance(accounts[4], accounts[3]);
      await expect(result).to.equal(10);
    });

    it('accounts[4] set again allowance for 10 tokens to accouts[3], it should not have changed', async () => {
      await token.connect(accounts[4]).approve(accounts[3], 10);

      const result = await token.allowance(accounts[4], accounts[3]);
      await expect(result).to.equal(10);
    });

    it('accounts[4] increase approval for 10 tokens to accouts[3], now it should be increased ', async () => {
      await token.connect(accounts[4]).increaseApproval(accounts[3], 10);

      const result = await token.allowance(accounts[4], accounts[3]);
      await expect(result).to.equal(20);
    });

    it('accounts[4] decrease approval for 5 tokens to accouts[3], now it should be descreased ', async () => {
      await token.connect(accounts[4]).decreaseApproval(accounts[3], 5);

      const result = await token.allowance(accounts[4], accounts[3]);
      await expect(result).to.equal(15);
    });

    it('accounts[4] try to approve 100 tokens for accounts[3]', async () => {
      const call = token.connect(accounts[0]).approve(ZeroAddress, 100);

      await expect(call).to.be.revertedWithCustomError(token, 'ERC20InvalidSpender')
        .withArgs(
          ZeroAddress
        );
    });

    it("should revert on approve when spender is address(0)", async function () {

      await expect(token.approve(ZeroAddress, 100))
          .to.be.revertedWithCustomError(token, "ERC20InvalidSpender")
          .withArgs(ZeroAddress);

    });
  });

  describe('transferBulk', () => {
    it('to many _tos', async () => {
      const _tos = [accounts[1], accounts[2]];
      const _values = [100];

      const call = token.connect(accounts[0]).transferBulk(_tos, _values);

      await expect(call).to.be.revertedWithCustomError(token, 'MismatchedArrays')
        .withArgs(
          2,
          1,
        );
    });

    it('to many _values', async () => {
      const _tos = [accounts[1]];
      const _values = [100, 100];

      const call = token.connect(accounts[0]).transferBulk(_tos, _values);

      await expect(call).to.be.revertedWithCustomError(token, 'MismatchedArrays')
        .withArgs(
          1,
          2,
        );
    });

    it('to many recipients', async () => {
      const _tos = [];
      for (let i = 0; i < 101; i++) {
        _tos.push(accounts[1]);
      }
      const _values = [];
      for (let i = 0; i < 101; i++) {
        _values.push(100);
      }

      const call = token.connect(accounts[0]).transferBulk(_tos, _values);

      await expect(call).to.be.revertedWithCustomError(token, 'TooManyRecipients')
        .withArgs(
          101,
          100,
        );
    });

    it('bulk value to high', async () => {
      const _tos = [];
      for (let i = 0; i < 100; i++) {
        _tos.push(accounts[1]);
      }
      const _values: BigUint64Array[] = [];
      for (let i = 0; i < 100; i++) {
        _values.push(1000000000000000000000000000000000n);
      }

      const call = token.connect(accounts[0]).transferBulk(_tos, _values);

      await expect(call).to.be.revertedWithCustomError(token, 'BulkValueTooHigh')
        // .withArgs(
        //   1,
        //   2,
        // )
        ;
    });

    it('check for BulkTransfer event', async () => {
      const _tos = [accounts[1].getAddress(), accounts[2].getAddress(), accounts[3].getAddress()];
      const _values = [10, 10, 20];

      const call = await token.connect(accounts[0]).transferBulk(_tos, _values);
      await expect(call).to.emit(token, 'BulkTransfer').withArgs(1, _tos.length);
    });

    it('should be successfull', async () => {
      let balance6 = await token.balanceOf(accounts[6]);
      await expect(balance6).to.equal(0);

      await token.connect(owner).transfer(accounts[6], 100);

      balance6 = await token.balanceOf(accounts[6]);
      await expect(balance6).to.equal(100);

      const _tos = [accounts[7], accounts[8], accounts[9]];
      const _values = [10, 20, 30];

      let balance7 = await token.balanceOf(accounts[7]);
      await expect(balance7).to.equal(0);

      let balance8 = await token.balanceOf(accounts[8]);
      await expect(balance8).to.equal(0);

      let balance9 = await token.balanceOf(accounts[9]);
      await expect(balance9).to.equal(0);

      const call = await token.connect(accounts[6]).transferBulk(_tos, _values);
      await expect(call).to.emit(token, 'BulkTransfer').withArgs(2, _tos.length);

      balance6 = await token.balanceOf(accounts[6]);
      await expect(balance6).to.equal(100 - _values.reduce((a, b) => a + b, 0));

      balance7 = await token.balanceOf(accounts[7]);
      await expect(balance7).to.equal(_values[0]);

      balance8 = await token.balanceOf(accounts[8]);
      await expect(balance8).to.equal(_values[1]);

      balance9 = await token.balanceOf(accounts[9]);
      await expect(balance9).to.equal(_values[2]);
    });

    it('send to zero address, contract and with too much value, will not be reverted because it use transferQuiet', async () => {

      const balance = await token.balanceOf(accounts[0]);

      const _tos = [ ZeroAddress, token.getAddress(), accounts[1]];
      const _values = [100, 100, balance + parseUnits('1') ];

      const call = token.connect(accounts[0]).transferBulk(_tos, _values);
      await expect(call).to.emit(token, 'BulkTransfer').withArgs(3, 0);

      const newBalance = await token.balanceOf(accounts[0]);
      expect(balance).to.equal(newBalance);

    });
  });

  describe('increaseApprovalBulk', () => {
    it('to many _spenders', async () => {
      const _spenders = [accounts[1], accounts[2]];
      const _values = [100];

      const call = token.connect(accounts[0]).increaseApprovalBulk(_spenders, _values);

      await expect(call).to.be.revertedWithCustomError(token, 'MismatchedArrays')
        .withArgs(
          2,
          1,
        );
    });

    it('to many _values', async () => {
      const _spenders = [accounts[1]];
      const _values = [100, 100];

      const call = token.connect(accounts[0]).increaseApprovalBulk(_spenders, _values);

      await expect(call).to.be.revertedWithCustomError(token, 'MismatchedArrays')
        .withArgs(
          1,
          2,
        );
    });

    it('_spenders more then max bulk', async () => {
      const _spenders = [];
      for (let i = 0; i < 101; i++) {
        _spenders.push(accounts[1]);
      }
      const _values = [];
      for (let i = 0; i < 101; i++) {
        _values.push(100);
      }

      const call = token.connect(accounts[0]).increaseApprovalBulk(_spenders, _values);

      await expect(call).to.be.revertedWithCustomError(token, 'TooManySpenders')
        .withArgs(
          101,
          100,
        );
    });

    it('bulk value to high', async () => {
      const _spenders = [];
      for (let i = 0; i < 100; i++) {
        _spenders.push(accounts[1]);
      }
      const _values: BigUint64Array[] = [];
      for (let i = 0; i < 100; i++) {
        _values.push(1000000000000000000000000000000000n);
      }

      const call = token.connect(accounts[0]).increaseApprovalBulk(_spenders, _values);

      await expect(call).to.be.revertedWithCustomError(token, 'BulkValueTooHigh')
        // .withArgs(
        //   1,
        //   2,
        // )
        ;
    });

    it('check for BulkApproval event', async () => {
      const _spenders = [accounts[1].getAddress(), accounts[2].getAddress(), accounts[3].getAddress()];
      const _values = [10, 10, 20];


      const call = await token.connect(accounts[0]).increaseApprovalBulk(_spenders, _values);
      await expect(call).to.emit(token, 'BulkApproval').withArgs(4, _spenders.length);
    });

    // this test will improve coverage
    it('send to zero address, contract and with too much value, will not be reverted because it use transferQuiet', async () => {

      const balance = await token.balanceOf(accounts[0]);

      const _spenders = [ ZeroAddress, token.getAddress(), accounts[1]];
      const _values = [100, 100, balance + parseUnits('1') ];

      const call = token.connect(accounts[0]).transferBulk(_spenders, _values);
      await expect(call).to.be.not.reverted;

      const newBalance = await token.balanceOf(accounts[0]);
      expect(balance).to.equal(newBalance);

    });

    it('should be successfull', async () => {
      const _spenders = [accounts[1], accounts[2], accounts[3]];
      const _values = [10, 20, 30];

      let allowance1 = await token.allowance(accounts[6], accounts[1]);
      await expect(allowance1).to.equal(0);

      let allowance2 = await token.allowance(accounts[6], accounts[2]);
      await expect(allowance2).to.equal(0);

      let allowance3 = await token.allowance(accounts[6], accounts[3]);
      await expect(allowance3).to.equal(0);

      const call = await token.connect(accounts[6]).increaseApprovalBulk(_spenders, _values);
      await expect(call).to.emit(token, 'BulkApproval').withArgs(6, _spenders.length);


      allowance1 = await token.allowance(accounts[6], accounts[1]);
      await expect(allowance1).to.equal(_values[0]);

      allowance2 = await token.allowance(accounts[6], accounts[2]);
      await expect(allowance2).to.equal(_values[1]);

      allowance3 = await token.allowance(accounts[6], accounts[3]);
      await expect(allowance3).to.equal(_values[2]);
    });
  });

  describe('increaseApproval', () => {

    it('accounts[5] increase approval accouts[3], truncating upon overflow', async () => {
      await token.connect(accounts[5]).increaseApproval(accounts[3], 115792089237316195423570985008687907853269984665640564039457584007913129639935n);

      const result = await token.allowance(accounts[5], accounts[3]);
      await expect(result).to.equal(115792089237316195423570985008687907853269984665640564039457584007913129639934n);
    });

    it("should revert on increasing approval when spender is address(0)", async function () {

      await expect(token.increaseApproval(ZeroAddress, 100))
          .to.be.revertedWithCustomError(token, "ERC20InvalidSpender")
          .withArgs(ZeroAddress);

    });

    it("on overflow approval should have (MaxUint256 - 1)", async function () {
      const initialAllowance = 1000000000000000000n;
      await token.connect(accounts[5]).approve(accounts[4], initialAllowance);

      let result = await token.allowance(accounts[5], accounts[4]);
      await expect(result).to.equal(initialAllowance);

      // Call increaseApproval with a delta that causes unchecked overflow with now error
      await expect(token.connect(accounts[5]).increaseApproval(accounts[4], MaxUint256))
        .to.be.not.reverted;

      result = await token.allowance(accounts[5], accounts[4]);
      await expect(result).to.equal(MaxUint256 - 1n);
    });

    it("on setting approval to MaxUint256 it should have (MaxUint256 - 1)", async function () {

      await expect(token.connect(accounts[6]).increaseApproval(accounts[4], MaxUint256))
        .to.be.not.reverted;

      const result = await token.allowance(accounts[5], accounts[4]);
      await expect(result).to.equal(MaxUint256 - 1n);
    });

  });


  describe('decreaseApproval', () => {
    it('accounts[5] decrease approval to accouts[3], truncating upon overflow', async () => {
      await token.connect(accounts[5]).decreaseApproval(accounts[3], 115792089237316195423570985008687907853269984665640564039457584007913129639935n);

      const result = await token.allowance(accounts[5], accounts[3]);
      await expect(result).to.equal(0);
    });

    it("should revert on decreasing approval when spender address address(0)", async function () {

      await expect(token.decreaseApproval(ZeroAddress, 100))
          .to.be.revertedWithCustomError(token, "ERC20InvalidSpender")
          .withArgs(ZeroAddress);

    });
  });

});
